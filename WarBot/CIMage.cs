﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using System.Drawing;
using System.Windows.Documents;


namespace WarBot
{
    class CIMage
    {
        

        public static FileStream DrawTable()
        {
            
            int size = Program.Participantes.List.Count;
            int columns = size /7 +1; // Max. 6 participants per on each column
            int rows = size / columns; // Split participants equally in all columns

            Bitmap escudo, dengue, jeringa, hiv, anemia, sinTACC;

            int anchoImg=18, altoImg=30;

            escudo = new Bitmap(Image.FromFile("Images/escudo.png"), anchoImg, altoImg );
            dengue = new Bitmap(Image.FromFile("Images/dengue.png"), anchoImg, altoImg);
            jeringa = new Bitmap(Image.FromFile("Images/jeringa.png"), anchoImg, altoImg);
            hiv = new Bitmap(Image.FromFile("Images/hiv.png"), anchoImg, altoImg );
            anemia = new Bitmap(Image.FromFile("Images/anemia.png"), anchoImg, altoImg);
            sinTACC = new Bitmap(Image.FromFile("Images/sinTACC.png"), anchoImg, altoImg);


            /**
             *
             * The number of columns and rows may not fit perfectly because of the number of participants
             * (for example, an odd number can't be perfectly splitted in 2 columns). In these scenarios,
             * an additional row is needed
             */
            if (columns * rows != size) rows++;

            int width = columns * 350;
            int height = rows * 60;
            Bitmap img = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            
            
            Graphics g2d = Graphics.FromImage(img);

          
            //g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            // NORMAL FONT
            Font aliveFont = new Font( "Arial", 18); 
            //g2d.setFont(aliveFont);
            
            //FontMetrics fm = g2d.getFontMetrics();

            // Comic sans font for DEAD participants
            Font deadFont = new Font(FontFamily.GenericSansSerif,  18);     //cambiar color con bursh
                                                             //Hashtable map = new Hashtable();


            // Background
            g2d.Clear(Color.White);
            SolidBrush backgr = new SolidBrush(Color.White);
            g2d.FillRectangle(backgr, 0, 0, width, height);

            Font font= null;
            Brush fontColor= null;

                                                    /*TEXT*/     
            for (int i = 0; i < columns; i++)
            {
                int verticalOffset = 0; // A new column begins
                for (int j = 0; j < rows; j++)
                {
                    int index = j + i * rows;
                    if (index < size)
                    {

                        Participante p = Program.Participantes.List[index];
                        String text = p.GetName();

                        // Set font
                        if (p.isAlive())
                        {
                            font = aliveFont;
                            fontColor= new SolidBrush(Color.Black);
                            
                        }
                        else
                        {
                            font = deadFont;
                            fontColor = new SolidBrush(Color.Red);
                        }
                        
                        if(!p.IsTargetable())
                        {
                            font = aliveFont;
                            fontColor = new SolidBrush(Color.Gray);
                        }

                        // Draw string
                        // Very long names are cut and ended with ...
                        if (text.Length > 34)
                            g2d.DrawString(text.Substring(0, 31) + "...", font, fontColor, i * 350, font.Height + verticalOffset);
                        else
                            g2d.DrawString(text, font, fontColor, i * 350, font.Height + verticalOffset);

                        float anchoTexto = g2d.MeasureString(text, font).Width;
                        float horizontalOffset = 5;

                        PointF punto = new PointF((i * 350) + anchoTexto + 10, font.Height + verticalOffset);
                        int o;

                        /*Dibujar escudos*/
                        for (o=0; o<p.ShieldCount; o++)
                        {
                            g2d.DrawImage(escudo, punto);
                            punto.X += horizontalOffset + anchoImg;
                        }

                        /*Dibujar jeringas*/
                        for ( o = 0; o < p.JeringaHIVCount; o++)
                        {
                            g2d.DrawImage(jeringa, punto);
                            punto.X += horizontalOffset + anchoImg;
                        }

                        /*Dibujar HIV*/
                        if (p.IsSidoso())
                        {
                            g2d.DrawImage(hiv, punto);
                            punto.X += horizontalOffset + anchoImg;
                        }

                        /*Dibujar anemia*/
                        if (p.IsAnemic())
                        {
                            g2d.DrawImage(anemia, punto);
                            punto.X += horizontalOffset + anchoImg;
                        }

                        /*Dibujar celiaquia*/
                        if (p.IsCeliaco ())
                        {
                            g2d.DrawImage(sinTACC, punto);
                            punto.X += horizontalOffset + anchoImg;
                        }

                        /*Dibujar dengue*/
                        if (p.DengueCount > 0)
                        {
                            g2d.DrawImage(dengue, punto);
                            punto.X += horizontalOffset + anchoImg;
                        }


                        verticalOffset += 50;
                    }
                }
            }
            g2d.Dispose();
            FileStream result = File.Create( Program.Participantes.contAlive + "remaining.png");
          
            try
            {
                img.Save(result, System.Drawing.Imaging.ImageFormat.Png);
                //img.Save(Program.Participantes.List.Count + "remaining.jpg");
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.StackTrace) ;
            }
            return result;
        }
    }
}

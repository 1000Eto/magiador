﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using TweetSharp;
//using System.Windows.Forms;
using System.Timers;

using System.IO;
using System.Threading;

namespace WarBot
{
    [Serializable]
    class Program
    {
        
        /* MODIFICAR LAS PROBABILIDADES DE EVENTOS IN GAME*/


        private static Participantes participantes;
        public static Participantes Participantes{ get { return participantes; } set { participantes = value; } }

        //private static List<Participante> cementerio = new List<Participante>();
        //public static List<Participante> Cementerio { get { return cementerio; } set { cementerio = value; } }

        #region Variables_Estaticas

        private static Double interval;
        private static System.Timers.Timer timer;
        private static Stopwatch stopwatch;
        private static Process process;
        private static ManejoArchivos fileManager;
        public static int contAlive { get; set; }
        private static int contEventos=0;

        private static bool testMode = true;

        #endregion


        static void Main(string[] args)
        {
            /*Carga de postulantes*/   
            process = new Process();

            participantes = new Participantes();
            participantes.contAlive = 0;

            //ordenar postulantes
            Ordenar o = new Ordenar();

            fileManager = new ManejoArchivos();

            

            Console.WriteLine("Los participantes estan listos: ");
            foreach (Participante _participante in participantes.List)
            {
                if (_participante.isAlive())
                {
                    Console.WriteLine(_participante.GetName());
                    //participantes.contAlive++;
                }
            }

            /*Estado: inactivo*/

            /*TIMER*/
            timer = new System.Timers.Timer();
            bool timerPaused = false;
            stopwatch = new Stopwatch();
            interval = 3600000;
            timer.Interval = interval;
            timer.AutoReset = true;

            timer.Elapsed += Activate;

            /*Inicializacion de maquina de estado*/
            Event.currentEvent = new Event("null");

            string entrada;

                                                   /*CONSOLA DE COMANDOS*/
            #region CONSOLA
        ConsolaComandos:                        

            while (true)
            {
                if (process.currentState == ProcessState.Terminated)
                    goto End;

                string modoPrueba = testMode ? "activado" : "desactivado";

                Console.Write("\t\t\t\tCOMANDOS\t " +
                "\n\n[Start]: Dar comienzo al bot" +
                "\n[Info]:  Mostrar informacion respecto al estado actual del bot" +
                "\n[Event]: Forzar un nuevo evento" +
                "\n[Clear]: Limpiar la pantalla de la consola" +
                "\n[StopTimer]:  Poner al bot en modo inactivo" +
                "\n[ChangeTimer]: Cambiar el intervalo de tiempo entre eventos" +
                "\n[ResetTImer]:  Reiniciar el timer a <" + (interval / 1000) / 60 + "> minutos" +
                "\n[Test]:  Activar/desactivar modo de prueba (modo de prueba " + modoPrueba + ")" +
                "\n[Exit]:   Finalizar la ejecicion del programa");

                Console.Write("\n\nCommand: ");

                Console.ForegroundColor = ConsoleColor.Blue;
                entrada = Console.ReadLine();
                Console.ResetColor();
                switch (entrada)
                {

                    case "Start":                                                                       //START
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"\n<{DateTime.Now}> - Bot Started");
                        Console.ResetColor();
                        process.MoveNext(Command.Begin);

                        SetInterval();
                        if (!timerPaused)
                            stopwatch.Restart();
                        else
                        {
                            stopwatch.Start(); timerPaused = false;
                        }

                        timer.Start();

                        break;
                    case "Info":                                                                        //INFO
                        goto Info;
                    case "Event":                                                                       //EVENT
                        if (process.currentState != ProcessState.Waiting)
                        {
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.WriteLine("\nERROR: Primero debe poner el bot en modo espera\n");
                            Console.ResetColor();
                            goto ConsolaComandos;
                        }

                        CallEvent();

                        break;
                    case "Clear":                                                                       //CLEAR
                        Console.Clear();
                        break;
                    case "StopTimer":                                                                   //STOPTIMER
                        timer.Stop();
                        stopwatch.Stop();
                        timerPaused = true;
                        process.MoveNext(Command.End);
                        break;
                    case "ChangeTimer":                                                                 //CHANGETIMER
                        Console.Write("Ingrese el nuevo valor del intervalo entre eventos (en minutos): ");
                        interval = (double.Parse(Console.ReadLine()) * 60000);
                        break;
                    case "ResetTimer":                                                                  //RESETTIMER
                        timer.Stop();
                        timer.Interval = interval;
                        timer.Start();
                        stopwatch.Restart();
                        break;
                    case "Test":
                        testMode = testMode ? false : true;     //cambio el estado de testMode
                        break;
                    case "Exit":                                                                        //EXIT
                        if (process.currentState != ProcessState.Inactive)
                        {
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.WriteLine("\nERROR: Primero debe poner el bot en modo inactivo\n");
                            Console.ResetColor();
                            goto ConsolaComandos;
                        }
                        process.MoveNext(Command.Exit);
                        goto End;

                    default:
                        Console.WriteLine("<<Comando invalido>>");
                        break;
                }
            }
        #endregion

            #region INFO
        Info:
            Console.Clear();

            System.Timers.Timer infoTimer = new System.Timers.Timer();
            infoTimer.Interval = 1000;
            infoTimer.AutoReset = true;
            infoTimer.Elapsed += DisplayInfo;
            infoTimer.Enabled = true;

            Console.ReadLine();

            infoTimer.Stop();
            Console.Clear();
            goto ConsolaComandos;
        #endregion

            #region END
        End:


            fileManager.WriteToBinaryFile<Participantes>("Participantes.bin", participantes);
            Console.WriteLine("\n\nPrograma finalizado");
            Console.Read();
            #endregion


        }

        private static void SetInterval()
        {

            TimeSpan tsSW = stopwatch.Elapsed;
            double ms;
            if (timer.Interval >= tsSW.TotalMilliseconds)
                ms = interval - tsSW.TotalMilliseconds;
            else
                ms = tsSW.TotalMilliseconds;



            timer.Interval = ms;

        }

        
        private static void Activate(Object source, ElapsedEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            contEventos++;
            Console.WriteLine($"\n\t\t\t\tNew event at {e.SignalTime} (n°{contEventos})");
            Console.ResetColor();

            process.MoveNext(Command.Tweet);
            CallEvent();
            process.MoveNext(Command.Hold);

            Console.WriteLine("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            if(participantes.contAlive == 1)
            {
                timer.Stop();
                stopwatch.Stop();
                process.MoveNext(Command.End);
            }
            else
            {
                SetInterval();
                stopwatch.Restart();
            }
           
        }

        private static void CallEvent()                      ///////////////////* LLAMAR EVENTO*//////////////////////
        {
            
            Event evento = new Event();

            string status = "";

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\n\\<<<<<<<<<<" + Event.currentEvent.GetText() + ">>>>>>>>>>\n");
            Console.ResetColor();

            status = Event.currentEvent.GetText() + $"\n({participantes.contAlive} participantes restantes)";


            fileManager.DrawImage();
            fileManager.WriteToBinaryFile<Participantes>("Participantes.bin", participantes); //sobrescribe el archivo anterior


            if (participantes.contAlive == 1)
            {
                status = status.Replace($"({participantes.contAlive} participantes restantes)", evento.GrandFinale());
            }

            if(!testMode)
                CTwitter.SendMediaTweet(status, 0);

        }

        private static void DisplayInfo(Object source, ElapsedEventArgs e)
        {
            Console.Clear();
            Console.WriteLine($"Time: {e.SignalTime}");

            /*PROCESO ACTUAL*/
            Console.WriteLine("\n<BOT> Actual state: " + process.currentState );    

            TimeSpan tsSW = stopwatch.Elapsed;
            double ms;
           
            ms = interval - tsSW.TotalMilliseconds;                                         /*miliesegunods restantes*/
           
            TimeSpan tRestante = new TimeSpan(((int)ms / 1000) / 3600, ((int)ms / 1000)/60, ((int)ms/1000)%60);

            /*ULTIMO EVENTO*/
            Console.WriteLine("Ultimo evento: ", Event.currentEvent.GetText());

            /*TIEMPO DESDE EL ULTIMO EVENTO*/
            Console.WriteLine("Time since last event: <{0}>", String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                        tsSW.Hours, tsSW.Minutes, tsSW.Seconds, tsSW.Milliseconds / 10));

            /*TIEMPO HASTA EL SIGUIENTE EVENTO*/
            Console.WriteLine("\nTime until next event: <{0}>", String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            tRestante.Hours, tRestante.Minutes, tRestante.Seconds, ms % 100));

            /*PARTICIPANTES CON VIDA*/
            Console.WriteLine("\nParticipantes: ");
            foreach (Participante _participante in participantes.List)
            {
                if(_participante.isAlive())
                    Console.WriteLine("\t"+_participante.GetName());
            }

            Console.Write("\n\n\n[Pulse cualquier tecla para volver a la consola de comandos]");
        }

    }
}

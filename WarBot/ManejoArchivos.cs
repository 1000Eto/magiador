﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace WarBot
{
    class ManejoArchivos
    {

        private FileStream image;


        public ManejoArchivos()
        {
            
            FileStream fp=null;

            try
            {
                fp = File.OpenRead("Participantes.bin");
            }
            catch
            {
                
            }

            if ( fp==null || Program.Participantes.contAlive==1 )
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("<<<<Se creara un nuevo archivo para participantes>>>>");
                Console.ResetColor();

                string[] lines = File.ReadAllLines("Postulantes.txt");

                

                foreach (string line in lines)
                {
                    Program.Participantes.List.Add(new Participante(line));
                    Program.Participantes.contAlive++;
                }

                WriteToBinaryFile<Participantes>("Participantes.bin", Program.Participantes); //El archivo Participantes.bin es creado

            }

            else
            {
                fp.Close();
                Program.Participantes = ReadFromBinaryFile<Participantes>("Participantes.bin");
            }

            image = CIMage.DrawTable();
            CTwitter.AddToImageList(image);
            image.Close();
            
        }

        ~ManejoArchivos()
        {
            Console.WriteLine("\nArchivos abiertos cerrados.");
            Console.ReadLine();
            WriteToBinaryFile<Participantes>("Participantes.bin", Program.Participantes);
        }

      
        public void WriteToBinaryFile<T>(string filePath, T objectToWrite, bool append = false)
        {
            using (Stream stream = File.Open(filePath, append ? FileMode.Append : FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objectToWrite);
            }
        }

        
        public T ReadFromBinaryFile<T>(string filePath)
        {
            using (Stream stream = File.Open(filePath, FileMode.Open))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                return (T)binaryFormatter.Deserialize(stream);
            }
        }

        public void DrawImage()
        {
            image = CIMage.DrawTable();
            CTwitter.AddToImageList(image);
            image.Close();
        }
        public FileStream GetImage()
        {
            return image;
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace WarBot
{
    class Event
    {
        //bool exito = false;
        public static Event currentEvent;


        private string text;
        private static float probRandom = 1;
        private static float probMurder = 0;



        //Venerea promoviendo la proteccion (clamidia, etc)
        //Rotavirus para bebes
        //Taenia saginata (lombriz solitaria)
        //Faringitis estreptocócica (deriva en fiebre romatica por estreptococo por tratamiento incorrecto con antibioticos)
        //muerte por pisada de golem bostero en concierto del indio solari
        //murio tratando de entender dark
        //astronautas
        //dato
        //cake human cake
        //tuberculosis
        //lepra~ 
        //cirrosis, alcoholismo
        //fiebre de sabado por la noche
        //Kun Aguero vs Cinthia Fernandez
        //cayo en una estafa lumiSpa Nu Skin
        //Nico Andreoli
        //nuevo orden mundial
        //streams de 72 hrs

        public Event()
        {
      
            //int i = CRandom.GetRandomValue(
            //    new CRandom.RandomSelection(0, 1, probMurder),
            //    new CRandom.RandomSelection(1, 2, probRandom)
            //);

            //foreach (Participante p in Program.Participantes.List)
            //    p.Check();                                                     //Chequeo cuando se crea un evento para llevar cuenta de ciertos status
            

            //if (i == 0)                           
            //    Event.currentEvent = new CMurder();
            //if (i == 1)                                                             
                Event.currentEvent = new CImprevisto();   
        }

        public Event(string s)
        {
            this.text = s;
        }
        
        ~Event()
        {
            Console.WriteLine("\nEvento finalizado.\n");
        }

        public void SetText(string s)
        {
            this.text = s;
        }
        public string GetText()
        {
            return this.text;
        }

       
        public static void ActualizarProb()
        {
            probRandom =   0.5f  - (Program.Participantes.contAlive / 7) * 5;
            probMurder =   1 - probRandom;
        }

        public string GrandFinale()
        {
            Participante campeon=null;
            foreach (Participante _participante in Program.Participantes.List)
                if (_participante.isAlive())
                {
                    campeon = _participante;
                }

            return $"Así, {campeon.GetName()} se quedó con la gloria de la victoria en la primera edición de 51d4 WarBot.";
        }
    }
}

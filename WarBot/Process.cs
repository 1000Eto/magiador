﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarBot
{
    public enum ProcessState
    {
        Inactive,
        Waiting,
        Active,
        Terminated
    }

    public enum Command
    {
        Begin,
        End,
        Hold,
        Tweet,
        Exit
    }
    public class Process
    {
        class StateTransition
        {
            readonly ProcessState currentState;
            readonly Command command;

            public StateTransition(ProcessState _currentState, Command _command)
            {
                currentState = _currentState;
                command = _command;
            }

            public override int GetHashCode()
            {
                return 17 + 31 * currentState.GetHashCode() + 31 * command.GetHashCode();
            }
            public override bool Equals(object obj)
            {
                StateTransition other = obj as StateTransition;
                return other != null && this.currentState == other.currentState && this.command == other.command;
            }
        }

        Dictionary<StateTransition, ProcessState> transitions;

        public ProcessState currentState { get; private set; }

        public Process()
        {
            currentState = ProcessState.Inactive;
            transitions = new Dictionary<StateTransition, ProcessState>
            {
                {new StateTransition(ProcessState.Inactive, Command.Exit),  ProcessState.Terminated },
                {new StateTransition(ProcessState.Inactive, Command.Begin), ProcessState.Waiting },
                {new StateTransition(ProcessState.Waiting, Command.End),    ProcessState.Inactive },
                {new StateTransition(ProcessState.Waiting, Command.Tweet),  ProcessState.Active },
                {new StateTransition(ProcessState.Active, Command.Hold),    ProcessState.Waiting },

            };
        }

        public ProcessState GetNext(Command _command)
        {
            StateTransition transition = new StateTransition(currentState, _command);
            ProcessState nextState;

            if(!transitions.TryGetValue(transition, out nextState))
                throw new Exception("Invalid transition: " + currentState + " -> " + _command);
            return nextState;
        }

        public ProcessState MoveNext(Command _command)
        {
            currentState = GetNext(_command);
            return currentState;
        }
    }
}

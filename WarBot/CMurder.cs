﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace WarBot
{
    class CMurder : Event
    {
        Participante offender, victim;
        StringBuilder sb = new StringBuilder("", 10);
        Random random = new Random();
        int i, o, t;

        #region Textos
        private string[] texts = new string[] {
            "{0} {2} a {1} con argumentos. {3}",
            "{0} {2} a {1}. {3}",       //asesino
            "{0} {2} poronga a {1}, {3}",
            "{0} {2} a {1} con un helado de Grido. {3}",
            "{0} {2} a {1}, {3}",   //jabon
            "{0} {2} el distanciamiento social y le tosió en la cara a {1}. {3}",
            "{0} {2} de cringe a {1} mandandole un video en el cual le habla a la cámara durante 1 hora. {3}",
            "{0} {2} a {1}. {3}"
        };
        private Dictionary<string, string> fallosTexts = new Dictionary<string, string>() {
            {"destruyó","{1} no se vió muy afectado por el hecho en vista de que {0} es fan de laje."},
            {"intentó asesinar","No se pudo, que se le va a hacer."},
            {"intentó hacer","poco sabía que {1} ya era una poronga."},
            {"intentó envenenar","{1} tuvó suerte y no ingirió ninguna babosa."},
            {"le tiró un jabón","menos mal que no es otaku."},
            {"no respetó","{0} no tiene coronavirus pero igual l@ hizo sintir re zarpad@."},
            {"intentó aniquilar","Decí que {1} es piola y borró el video cuando vió lo que duraba."},
            {"le ofreció un globito","No l@ mató pero le dió una buena lección de física."}
        };
        private Dictionary<string, string> murderTexts = new Dictionary<string, string>() {
            {"destruyó","{1} no pudo soportar la verguenza y se quitó la vida."},
            {"asesinó","Corta."},
            {"hizo","recontra poronga l@ hizo."},
            {"envenenó",""},
            {"le tiró un jabón","quien murió por otaku."},
            {"no respetó","Rip {1}."},
            {"aniquiló",""},
            {"le ofreció un globito","Acto seguido, el chupacabras mató a {1}."},

        };
        #endregion

        //jubilado enagenado
        public CMurder()
        {
            do
            {
                i = random.Next(0, Program.Participantes.List.Count);

            } while (Program.Participantes.List[i].isAlive() == false || Program.Participantes.List[i].IsTargetable() == false);


            do
            {
                o = random.Next(0, Program.Participantes.List.Count);

            } while (i == o || Program.Participantes.List[o].isAlive() == false || Program.Participantes.List[o].IsTargetable() == false);


            offender = Program.Participantes.List[i];
            victim = Program.Participantes.List[o];

            t = random.Next(0, texts.Length);
            string texto = texts[t];



            string resultado = offender.TryToKill(victim);
            string verb;
            StringBuilder compl = new StringBuilder();


            /*Evento HIV*/
            if (resultado == "hiv")
            {
                if (victim.isAlive())
                    this.SetText($"{offender.GetName()} le clavó un jeringazo sidoso a {victim.GetName()}. No l@ mat@, pero le tiró las defensas a la basura." +
                        $"\n\n*-1 jeringa con hiv para {offender.GetName()}" +
                        $"\n*{victim.GetName()} contrajo sida (reduce los escudos a 0 y anula cualquiera que gane dentro de los siguientes 5 eventos)");
                else
                    this.SetText( $"{offender.GetName()} le clavó un jeringazo sidoso a {victim.GetName()} y aprovechó sus bajas defensas para matarl@." +
                        $"\n\n*-1 jeringa con hiv para {offender.GetName()}");

            }

            /*Asesinato fallido*/
            if (victim.isAlive())
            {
                verb = fallosTexts.ElementAt(t).Key;
                compl.AppendFormat(fallosTexts[verb], offender.GetName(), victim.GetName());

                sb.AppendFormat(texto, offender.GetName(), victim.GetName(), verb, compl.ToString());

                sb.Append("\n\n" + resultado);
                this.SetText(sb.ToString());
            }

            /*Asesinato exitoso*/
            verb = murderTexts.ElementAt(t).Key;
            compl.AppendFormat(murderTexts[verb], offender.GetName(), victim.GetName());

            sb.AppendFormat(texto, offender.GetName(), victim.GetName(), verb, compl.ToString());

            this.SetText(sb.ToString());

        }

    }

    
}

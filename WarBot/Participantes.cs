﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarBot
{
    [Serializable]
    class Participantes
    {
        private List<Participante> list;

        public List<Participante> List { get { return list; } set { list = value; } }

        public int contAlive { get; set; }

        public int contDengue { get; set; }

        public Participantes()
        {
            list = new List<Participante>();
        }
    }

    
}

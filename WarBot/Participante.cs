﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WarBot
{
    [Serializable]
    class Participante
    {
        private string name = "";

        private bool alive = true;

        /*estados*/

        Dictionary<string, bool> estados = new Dictionary<string, bool>()
        {
            {"anemico", false},
            {"celiaco", false},
            {"sidoso", false},
            {"dengue", false},
        };


        //private bool celiaco = false;
        //private bool targetable = true;
        //private bool sidoso = false;
        //private bool dengue = false;

        private bool targetable = true;
        private byte contSida = 0;
        private int kidnapped = 0;
        private byte dengueCount = 0;

        Dictionary<string, byte> items = new Dictionary<string, byte>()
        {
            {"escudos", 0},
            {"jeringaHIV", 0},
        };


        public byte ShieldCount { get { return items["escudos"]; } set { items["escudos"] = value; } }
        public byte JeringaHIVCount { get { return items["jeringaHIV"]; } set { items["jeringaHIV"] = value; } }
        public byte DengueCount { get { return dengueCount; }  }



        public Participante(string _name)
        {
            this.name = _name;

            //if(this.name == "Tomas Mileto" || this.name == "Stefano Piperno")
            //{
            //    this.escudos = 2;
            //    this.jeringaHIV = 1;
            //    this.anemico = true;
            //    this.sidoso = true;
            //    this.dengue = 1;
            //    this.celiaco = true;
                
            //}
        }

        

        public bool isAlive()
        {
            return this.alive;
        }

        #region ITEM FUNC
        public bool HasItems()
        {
            bool ret = false;

            foreach (KeyValuePair<string, byte> item in items)
            {
                if (item.Value != 0)
                    ret = true;
            }

            return ret;
        }

        public bool HasItem(int i)
        {
            bool ret;

            ret = items.ElementAt(i).Value > 0 ? true : false;

            return ret;
        }
        public string Item(int i)
        {
            string ret = "";
            switch (items.ElementAt(i).Key)
            {
                case "escudos":
                    ret = "escudo";
                    break;
                case "jeringaHIV":
                    ret = "jeringa con HIV";
                    break;
            }
            return ret;
        }
        public void Item(int i, bool incremento)
        {
            if (incremento)
                items[items.ElementAt(i).Key]++;
            else
                items[items.ElementAt(i).Key]--;
        }
        #endregion

        public bool HasADisease()
        {
            bool ret;

            ret = estados.ContainsValue(true);  //De tener un estado en verdadero devuleve verdadero, si no, falso
        
            return ret;
        }
        public string TryToKill(Participante _victima)     
        {
            if (this.items["jeringaHIV"] > 0)
            {
                
                if(_victima.ShieldCount == 0)
                {
                    if(this.estados["anemico"])
                    {
                        _victima.Die();
                        items["jeringaHIV"]--;
                        return "hiv";
                    }
                }
                else
                {
                    if (_victima.ShieldCount < 2)
                    {
                        _victima.Die();
                    }
                    else
                    {
                        DestroyShields(_victima, _victima.ShieldCount);
                        _victima.GetsSida();
                    }

                    items["jeringaHIV"]--;
                    return "hiv";
                }
                

            }
            if (this.estados["anemico"])
            {
                this.estados["anemico"] = false;
                return $"*La anemia de {this.name} desaparece. El universo es muy confuso realmente";
            }

            if(_victima.ShieldCount>0)
            {   
                DestroyShields(_victima, 1);
                return $"-1 escudo para {_victima.name}";
            }


            _victima.Die();
            return "";
        }
        public void DestroyShields(Participante _victima, byte _shieldAmount)
        {

            for (int i = 0; i < _shieldAmount && _victima.ShieldCount > 0; i++)
                _victima.ShieldCount--;
        }
        public void Die()
        {
            this.alive = false;
            this.Reset();
            Program.Participantes.contAlive--;

            //Program.Participantes.List.Remove(this);
            //Program.Cementerio.Add(this);
        }
        public void Revive()
        {
            this.alive = true;
            Program.Participantes.contAlive++;
            //Program.Participantes.List.Add(this);
            //Program.Cementerio.Remove(this);
        }

        #region ENFERMEDADES
        public void GetsAnemia() { this.estados["anemico"] = true; }
        public bool IsAnemic() { return this.estados["anemico"]; }

        public void GetsCeliaquia() { this.estados["celiaco"] = true; }
        public bool IsCeliaco() { return this.estados["celiaco"]; }

        public void GetsSida()
        {
            this.estados["sidoso"] = true;
            this.contSida = 3;
        }
        public bool IsSidoso() { return this.estados["sidoso"]; }
        public string GetsDengue()
        {
            this.dengueCount++;

            if (this.dengueCount == 1)
            {
                this.estados["dengue"] = true;

                return "Oh no! " + this.name + " se durmió con la ventana abierta e hizo una orgía con una familia de Aedes" +
                    " aegyptis. Contrajo dengue.\n\n*Si contrae otra cepa del virus muere";

            }


            this.Die();

            return "Dios me libre y me guarde! " + this.name + " volvió a dormirse con la ventana abierta y esta vez hizo" +
                "una orgía con una familia extendida de Aedes aegyptis (abuelos, tíos, primos, primos segundos)" +
                " que llevaba otra cepa del virus. Contrajo dengue hemorragico y murió.";


        }
        #endregion


        public string GetName()
        {
            return this.name;
        }

        public void NotTargetable() {

            this.kidnapped = Program.Participantes.contAlive/3  +1;
            targetable = false;
        }
        public void Targetable(){
            targetable = true;
            kidnapped = 0;
        }

        public bool IsTargetable() { return targetable; }

        public void Reset()
        {
            this.estados["anemico"] = false;
            this.estados["celiaco"] = false;
            this.estados["sidoso"] = false;
            this.estados["dengue"] = false;

            targetable = true;
            dengueCount = 0;
            kidnapped = 0;
            items["escudos"] = 0;
            items["jeringaHIV"] = 0;
        }
        public void Check()
        {
            if (this.targetable==false)
            {
                kidnapped--;

                if (kidnapped == 0)
                    this.Targetable();
            }

            if( this.estados["sidoso"])
            {
                contSida--;

                if (contSida == 0)
                    this.estados["sidoso"] = false;
            }
        }
    }


}

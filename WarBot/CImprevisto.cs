﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;
using System.Text;

namespace WarBot
{

    class CImprevisto : Event
    {
        //public delegate string EventFunction(Participante p, Event e);

        private static float common = 0.50f;
        private static float uncommon = 0.28f;
        private static float rare = 0.17f;
        private static float epic = 0.05f;


        
        private static List<Func<Participante,Event, string>> commonEvents = new List<Func<Participante, Event, string>>() {
            Escudo,     //50%
        };

        private static List<Func<Participante, Event, string>> uncommonEvents = new List<Func<Participante, Event, string>>() {
            Anemia,     //28%
        };
        //Pan,      //14%
        private static List<Func<Participante, Event, string>> rareEvents = new List<Func<Participante, Event, string>>() {
            HIV,                //3.5%
            Dengue,             //3.5%
            //"Celiaquia",
            Pomberito,          //3.5%      se puede agregar algo albinezco
            Casino,             //3.5%
            Bendicion,          //3.5%
        };
        private static List<Func<Participante, Event, string>> epicEvents = new List<Func<Participante, Event, string>>() {
            MuerteSubita,       //2%
            Revivir,            //2%
            //"Muerte doble",   //2%
            //uno reverse card
        };

        private static List<Func<Participante, Event, string>> events = new List<Func<Participante, Event, string>>();
       
              
        #region TEXTOS
        private static string[] escudosTexts = new string[] {
            "{0} encontró una linea de merca y no dudó ni un segundo en raquetearsela.",
            //"{0} entendió todo lo que se dictó en la clase online y está al día con la facultad. Está re solid@.",
            "{0} desayunó Actimel.",
            "{0} desayunó Actimel.",
            "{0} desayunó danonino.",
            "{0} unió fuerzas con Carlos Tevez para vencer a Pachorra.",
            "{0} encontró un barbijo.",
            "{0} se liquidó un pote de comprimidos Centrum.",
        };
        private static string[] nadaTexts = new string[] {
            "{0} rompió la caurentena para ponerla. No pasó nada pero terrible hdp, escrachen.",
            "{0} se hizo evangelista.",
            "La cosa estaba tranqui. {0} rompió el silencio para decir que la casa de papel es una poronga. Todos coincidieron.",
            "{0} se cruzó a Alejo Devil y tuvo la charla más neutra de su vida.",
            "{0} se hinchó las bolas con los memes de los carpinchos. Terrible hdp, escrachen.",
            "No pasó nada interesante, como en la vida de {0}.",
            "{0} leyó 01110000 01110101 01110100 01101111 00100000 01100101 01101100 00100000 01110001 01110101 01100101 00100000 01101100 01100101 01100101 en ASCII.",
        };
        #endregion
            
        public CImprevisto():base("nada")
        {
            UpdateEvents();


            /*Llenar vector de indices para separar por rareza*/
            int[] indices = {
                commonEvents.Count,
                commonEvents.Count + uncommonEvents.Count,  
                commonEvents.Count + uncommonEvents.Count + rareEvents.Count,
                commonEvents.Count + uncommonEvents.Count + rareEvents.Count + epicEvents.Count,
            };
           
            int i = CRandom.GetRandomValue(
                new CRandom.RandomSelection(0, indices[0], common),
                new CRandom.RandomSelection(indices[0], indices[1], uncommon),
                new CRandom.RandomSelection(indices[1], indices[2], rare),
                new CRandom.RandomSelection(indices[2], indices[3], epic)
             );

            Participante p = Program.Participantes.List[CRandom.GetRandomValue(0,Program.Participantes.List.Count)];

            this.SetText(CImprevisto.events[i](p, this));
          
        }

        #region Eventos

        public static string Escudo (Participante participante, Event e)
        {
            int p;

            while (participante.isAlive() == false || participante.IsTargetable() == false)
            {
                p = CRandom.GetRandomValue( 0, Program.Participantes.List.Count );
                participante = Program.Participantes.List[p];
            }


            if (participante.IsSidoso())
                return $"{participante.GetName()} hubiese ganado un escudo si no tuviera sida. Una lástima.";

            participante.ShieldCount++;


            int t = CRandom.GetRandomValue(0, escudosTexts.Length);

            StringBuilder sb = new StringBuilder();

            sb.AppendFormat(escudosTexts[t] + "\n\n *+1 escudo (" + participante.ShieldCount + " en total)", participante.GetName());

            return sb.ToString();
        }
        public static string Anemia(Participante participante, Event e)
        {
            int p;

            while (participante.IsAnemic() || participante.isAlive() == false || participante.IsTargetable() == false)
            {
                p = CRandom.GetRandomValue(0, Program.Participantes.List.Count);
                participante = Program.Participantes.List[p];

            }

            participante.GetsAnemia();

            string s = participante.GetName() + " llevaba varios dias sin comerse un buen cacho de carne y sufrió una brusca disminución de" +
                " vitamina B12. Contrajo anemia megaloblástica.\n\n*fallará en su próximo intento de asesinato";
            return s;
        }
        //public static string Pan(Participante participante, Event e)
        //{
        //    int p;

        //    while (participante.isAlive() == false || participante.IsTargetable() == false)
        //    {
        //        p = CRandom.GetRandomValue(0, Program.Participantes.List.Count);
        //        participante = Program.Participantes.List[p];
        //    }

        //    string text = $"{participante.GetName()} tropezó y cayó sobre una pileta llena de figazas de pan.";

        //    if (participante.IsCeliaco())
        //    {
        //        participante.Die();
        //        return text + $" Debido a su enfermedad, {participante.GetName()} terminó muriendo por complicaciones varias. Celiac@ qlia@.";
        //    }

        //    return text + $" {participante.GetName()} quedó muy confundid@ pero también con vida ya que no es un/a celiac@ qlia@.";
        //}
        public static string Dengue(Participante participante, Event e)
        {
            int p;

            while (participante.isAlive() == false || participante.IsTargetable() == false)
            {
                p = CRandom.GetRandomValue(0, Program.Participantes.List.Count);
                participante = Program.Participantes.List[p];
            }

            string s = participante.GetsDengue() + "\n ";

            return s;
        }
        public static string HIV(Participante participante, Event e)
        {
            int p;

            while (participante.isAlive() == false || participante.IsTargetable() == false)
            {
                p = CRandom.GetRandomValue(0, Program.Participantes.List.Count);
                participante = Program.Participantes.List[p];

            }

            participante.JeringaHIVCount++;

            string s = participante.GetName() + " encontró una jeringa usada por un drogadicto con 51d4 y la guardó." +
                "\n\n*Se usa en la primera víctima que esté escudada. Esta contrae 51d4 si tiene 2 o más escudos , o muere si tiene sólo 1 [precede a la anemia]";

            return s;

        }

        //public static string Celiaquia(Participante participante, Event e)
        //{
        //    int p;

        //    while (participante.isAlive() == false || participante.IsTargetable() == false || participante.IsCeliaco() == true)
        //    {
        //        p = CRandom.GetRandomValue(0, Program.Participantes.List.Count);
        //        participante = Program.Participantes.List[p];
        //    }

        //    participante.GetsCeliaquia();

        //    return $"En un giro de eventos desafortunado {participante.GetName()} fue diagnosticad@ con celiaquía. Sería una desgracia si llegase" +
        //        $" a caer en una pileta llena de figazas de pan.";
        //}
        public static string Pomberito(Participante participante, Event e)
        {
            int p;
            while (participante.isAlive() == false || participante.IsTargetable() == false)
            {
                p = CRandom.GetRandomValue(0, Program.Participantes.List.Count);
                participante = Program.Participantes.List[p];
            }

            participante.NotTargetable();

            return $"En un giro de eventos criollos y confusos el pomberito secuestró a {participante.GetName()}." +
                $"\n\n*No podrá participar ni ser asesinado durante los siguientes {Program.Participantes.contAlive / 3} eventos";
        }
        public static string Casino (Participante participante, Event e)
        {
            int p, i;
            string s="";

            while (participante.isAlive() == false || participante.IsTargetable() == false || participante.HasItems() == false )
            {
                p = CRandom.GetRandomValue(0, Program.Participantes.List.Count);
                participante = Program.Participantes.List[p];
            }

            do          //Seleccionar uno de los objetos
            {
                i = CRandom.GetRandomValue(
                new CRandom.RandomSelection(0, 1, 0.7f),
                new CRandom.RandomSelection(1, 2, 0.3f)
                );

            } while (participante.HasItem(i) == false);

            int r = CRandom.GetRandomValue(                     //Determinar si el resultado es positivo o negativo
                new CRandom.RandomSelection(0, 1, 0.6f),
                new CRandom.RandomSelection(1, 2, 0.4f)
                //agregar jackpot
                );

            if( r == 0 )
            {
                participante.Item(i, incremento: true);
                s = $"{participante.GetName()} fue al casino flotante y en un acto de ludopatía apostó 1 {participante.Item(i)} al rojo." +
               $" 32 Rojo! Enhorabuena!"+
               $"\n\n+1 {participante.Item(i)}";
            }
            if (r == 1)
            {
                participante.Item(i, incremento: false);
                s = $"{participante.GetName()} fue al casino flotante y en un acto de ludopatía apostó 1 {participante.Item(i)} al rojo." +
              $" 13 Negro! UUUh!" +
              $"\n\n-1 {participante.Item(i)}";
            }

            

            return s;
        }
        public static string MuerteSubita(Participante participante, Event e)
        {
            int p;
            while (participante.isAlive() == false || participante.IsTargetable() == false)
            {
                p = CRandom.GetRandomValue(0, Program.Participantes.List.Count);
                participante = Program.Participantes.List[p];
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Oh no! {0} derramó vino tinto sobre la camisa de un rugbier.", participante.GetName());
            if (participante.ShieldCount > 0)
            {
                sb.Append($" Por suerte primó la paz y pudieron llegar a un acuerdo hablando.\n\n*-1 escudo para {participante.GetName()}");
                participante.ShieldCount--;

                return sb.ToString();
            }

            sb.Append(" Acto seguido ligó una lluvia de piñas hormonadas y murió en el acto.");

            return sb.ToString();
        }
        public static string Revivir(Participante participante, Event e)
        {
            int p;
            while (participante.isAlive())
            {
                p = CRandom.GetRandomValue(0, Program.Participantes.List.Count);
                participante = Program.Participantes.List[p];
            }

            participante.Revive();

            return participante.GetName() + " ha sido revivid@ con medicina quechua. Achalay!";
        }

        public static string Bendicion(Participante participante, Event e)
        {
            return null;
        }
        #endregion



        public void UpdateEvents()
        {
            CImprevisto.events.Clear();

            CImprevisto.events.AddRange(commonEvents);
            CImprevisto.events.AddRange(uncommonEvents);
            CImprevisto.events.AddRange(rareEvents);
            CImprevisto.events.AddRange(epicEvents);
        }


    }
}


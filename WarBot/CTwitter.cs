﻿using System;
using System.Collections.Generic;
using System.Text;
using TweetSharp;
using System.Timers;
using System.Net;
using System.IO;

namespace WarBot
{
    class CTwitter
    {
        private static string customer_key = "FQFt4BOK9HUnhOdFj1pShsMRL";
        private static string customer_key_secret = "G1N7XFf9ch4M0MzuDnRKD2p3vlBnhXqNXNR1LGdLLqm4HS3BTK";
        private static string access_token = "1251582141072314370-9Abuv9sFhoh702lBqilHHsTNLOTjq0";
        private static string access_token_secret = "AS4p1SiIUeWwdEtI8cxVeivk5RLxEIa1FzHN2w9WBFCiQ";

        private static TwitterService service = new TwitterService(customer_key, customer_key_secret, access_token, access_token_secret);

        public static int currentImageID = 0;
        private static List<string> imageList = new List<string> { } ;


        public static void SendTweet(string _status)
        {
            service.SendTweet(new SendTweetOptions { Status = _status }, (tweet, response) =>
            {
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"<{DateTime.Now}> - Tweet Enviado!");
                    Console.ResetColor();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"<ERROR>" + response.Error.Message);
                    Console.ResetColor();
                }

            });

        }

        public static void AddToImageList(FileStream _image)
        {
            imageList.Insert(0, _image.Name);
        }

        public static void SendMediaTweet(string _status, int imageID)
        {
            using (var stream = new FileStream(imageList[imageID], FileMode.Open))
            {   
                try
                {
                    service.SendTweetWithMedia(new SendTweetWithMediaOptions
                    {
                        Status = _status,
                        Images = new Dictionary<string, Stream> { { imageList[imageID], stream } }

                    });
                }
                catch (Exception e)
                {
                    Console.WriteLine("Supuesto error con imagen. Error: " + e.Message);
                }

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"<{DateTime.Now}> - Tweet Enviado!");
                Console.ResetColor();

                //if ((currentImageID + 1) == imageList.Count)
                //{
                //    Console.ForegroundColor = ConsoleColor.Red;
                //    Console.WriteLine("<BOT> - End of Image Array");
                //    Console.ResetColor();
                //    currentImageID = 0;
                //}
                //else
                //{
                //    currentImageID++;
                //}


            }
        }
    }
}

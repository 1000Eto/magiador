﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarBot
{
    static class CRandom
    {
        public struct RandomSelection
        {
            private int minValue;
            private int maxValue;
            public float probability;

            public RandomSelection(int minValue, int maxValue, float probability = 1)
            {
                this.minValue = minValue;
                this.maxValue = maxValue;
                this.probability = probability;
            }

            public int GetValue() { return new Random().Next(minValue, maxValue ); }
        }
        public static int GetRandomValue(params RandomSelection[] selections)
        {
            float rand = (float)new Random().NextDouble();
            float currentProb = 0;
            foreach (var selection in selections)
            {
                currentProb += selection.probability;
                if (rand <= currentProb)
                    return selection.GetValue();
            }

            //will happen if the input's probabilities sums to less than 1
            //throw error here if that's appropriate
            return -1;
        }

        public static int GetRandomValue(int min, int max)
        {
            return new Random().Next(min, max); 
        }

    }
}
